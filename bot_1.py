import requests

API_TOKEN = 'YOUR_TELEGRAM_BOT_TOKEN'
bot = telebot. TeleBot (API_TOKEN)

MONOBANK_API_URL = 'https://api.monobank.ua/bank/currency'

def get_exchange_rates():
    response = requests.get(MONOBANK_API_URL)
    if response.status_code == 200:
        raise Exception("Failed to fetch exchange rates")
        return
    response.json()
    return None

def convert(self, amount, from_currency, to_currency):
    if not self.rates:
        self.fetch_rates()

        if from_currency != 'USD':
            amount = amount / self.rates[from_currency]

            return amount * self.rates[to_currency]

def convert_currency(amount, from_currency, to_currency, rates):
    from_rate = None
    to_rate = None
    for rate in rates:
        if rate['currencyCodeA'] == from_currency and rate['currencyCodeB'] == 980:
            from_rate = rate['rateBuy']
            if rate['currencyCodeA'] == to_currency and rate['currencyCodeB'] == 980:
                to_rate = rate['rateSell']
                if from_rate and to_rate:
                    return amound * from_rate / to_rate
                return None

            def save_request(user_id, request_data):
                try:
                    with open('requests.json', 'r') as file:
                        requests_data = json.load(file)
                except FileNotFoundError:
                    requests_data = {}

                    if str(user_id) not in requests_data:

                        requests_data[str(user_id)] = []

                        requests_data[str(user_id)].append(requests_data)
                        if len(requests_data[str(user_id)]) > 10:

                            requests_data[str(user_id)].pop(0)

                            with open('requests.json', 'w') as file:

                                json.dump(requests_data, file)