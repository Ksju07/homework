import telebot

load_dotenv()
bot = telebot.TeleBot(os.getenv("TOKEN"))
currency = CurrencyConvertor(os.getenv("API"))
amount = 0
from_currency = ""
to_currency = ""

def save_last_requests(request):
    filename = "last_requests.json"
    if os.path.exists(filename):
        with open(filename,"r") as file:
            data = json.load(file)
    else:
        data = []

        data.append(request)
        if len(data) > 10:
            data.pop(0)

            with open(filename,"w") as file:
                json.dump(data, file)

@bot.message_handler(commands=['start', 'help'])
def send_welcome(message):
    bot.reply_to(message, "Привіт! Я - бот для конвертації валют. Введіть суму та оберіть вихідну та цільову валюту для конвертації, введіть команду /convert")

    @bot.message_handler(commands=["convert"])
    def convert(message):
        bot.send_message(message.chat.id, "Оберіть будь ласка валюту з якої конвертуєте")
        markup = types.InlineKeyboardMarkup(row_width=2)
        btn1 = types.InlineKeyboardButton("UAH", callback_data="from_uah")
        btn2 = types.InlineKeyboardButton("USD", callback_data="from_usd")-++
        btn3 = types.InlineKeyboardButton("EUR", callback_data="from_eur")
        btn4 = types.InlineKeyboardButton("Введіть потрібну валюту", callback_data="from_else")
        markup.add(btn1, btn2, btn3, btn4)
        bot.send_message(message.chat.id, "Оберіть потрібну валюту для конвертації, reply_markup=markup")

        @bot.callback_query_handler(func=lambda callback: callback.data.startswith("from_"))
        def callBackFrom(callback):
            global from_currency
            from_currency = callback.data[5:].upper()

            if from_currency == "ELSE":
                bot.send_message(callback.message.chat.id, "Оберіть потрібну валюту")
                bot.register_next_step_handler(callback.message, specify_currency)
            else:
                bot.send_message(callback.message.chat.id, f"Вибрано валюту:{from_currency}")
                bot.send_message(callback.message.chat.id, "Введіть суму для конвертації")
                bot.register_next_step_handler(callback.message, get_amount)

                def specify_currency(message):
                    global from_currency
                    from_currency = message.text.strip().upper()
                    bot.send_message(message.chat.id, f"Вибрано валюту:{from_currency}")
                    bot.send_message(message.chat.id, "Введіть суму для конвертації")
                    bot.register_next_step_handler(message, get_amount)

                    def get_amount(message):
                        global amount
                        try:
                            amount = float(message.text.strip())
                        bot.send_message(message.chat.id, "Оберіть будь ласка валюту для конвертації")
                        markup = types.InlineKeyboardMarkup(row_width=2)
                        btn1 = types.InlineKeyboardButton("UAH", callback_data="to_uah")
                        btn2 = types.InlineKeyboardButton("USD", callback_data="to_usd")
                        btn3 = types.InlineKeyboardButton("EUR", callback_data="to_eur")
                        btn4 = types.InlineKeyboardButton("Інша валюта", callback_data="to_else")
                        markup.add(btn1, btn2, btn3, btn4)
                        bot.send_message(message.chat.id, "Оберіть потрібну валюту для конвертації", reply_markup=markup)
                        except ValueError:
                        bot.send_message(message.chat.id, "Будь ласка, введіть коректну суму")
                        bot.register_next_step_handler(message, get_amount)

@bot.callback_query_handler(func=lambda callback: callback.data.data.startswith("to_"))
def callBackTo(callback):
    global to_currency
    to_currency = callback.data[3:].upper()

    if to_currency == "ELSE":
        bot.send_message(callback.message.chat.id,"Зазначте потрібну валюту")
        bot.register_next_step_handler(callback.message, specify_to_currency)
    else:
        convert_currency(callback.message)

        def specify_to_currency(message):
            global to_currency
            to_currency = message.text.strip().upper()
            convert_currency(message)

def convert_currency(message):
    global amount, from_currency, to_currency
    try:
        result = currency.convert(amount, from_currency, to_currency)
        bot.send_message(message.chat.id, f"{amount}{from_currency} = {result:.2f}{to_currency}")
        save_last_requests({"from_currency":from_currency,"to_currency":to_currency, "amount":amount, "result": result})
    except Exception as e:
        bot.send_message(message.chat.id, f"Помилка конвертації: {e}")

    user_id = message.from_user.id
    text = message.text.strip()
    try:
        amount, from_currency, to_currency = text.split()
        amount = float(amount)
        from_currency = int(from_currency)
        to_currency = int(to_currency)
        rates = get_exchange_rates()
        if rates:
            converted_amount = convert_currency(amount,from_currency, to_currency,rates)
            if converted_amount:
                response = f"{amount} {from_currency} = {converted_amount:.2f} {to_currency}"

                save_request(user_id, {'amount': amount, 'from_currency': from_currency, 'to_currency': to_currency, 'result': converted_amount})
            else:
                response = "Не вдалося знайти курс обміну для вкзаних валют."
        else:
                response = "Не вдалося отримати курси валют."
    except ValueError:
        response = "Будь ласка, введіть правильну суму та коди валют. Формат: <сума> <код вихідної валюти> <код цільової валюти>"
        bot.reply_to(message, response)

        bot.polling(non_stop=True)